module.exports = {
    projects: [
        "projects/shared/jest.config.js",
        "projects/server/jest.config.js",
        "projects/frontend/jest.config.js",
    ],    
    verbose: true,
    collectCoverage: true,
};
