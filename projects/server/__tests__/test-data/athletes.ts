import { Athlete, CompetitionModes, LiftStatus } from "shared";
export const LiftOrderData: Athlete[] = [
    {
        GroupName: "G1",
        Firstname: "",
        Lastname: "",
        Gender: "M",
        Lot: 1,
        Lifts: {
            Squat: [
                {
                    Requested: 101,
                    Status: LiftStatus.NOT_ATTEMPTED
                },
                {
                    Requested: 102,
                    Status: LiftStatus.NOT_ATTEMPTED
                }
            ],
            Bench: [],
            Deadlift: []
        }
    },
    {
        GroupName: "G1",
        Firstname: "",
        Lastname: "",
        Gender: "M",
        Lot: 2,
        Lifts: {
            Squat: [
                {
                    Requested: 100,
                    Status: LiftStatus.NOT_ATTEMPTED
                },
                {
                    Requested: 101,
                    Status: LiftStatus.NOT_ATTEMPTED
                }
            ],
            Bench: [],
            Deadlift: []
        }
    },
    {
        GroupName: "G1",
        Firstname: "",
        Lastname: "",
        Gender: "M",
        Lot: 3,
        Lifts: {
            Squat: [],
            Bench: [
                {
                    Requested: 100,
                    Status: LiftStatus.NOT_ATTEMPTED
                },
                {
                    Requested: 101,
                    Status: LiftStatus.NOT_ATTEMPTED
                }
            ],
            Deadlift: []
        }
    },
    {
        GroupName: "G1",
        Firstname: "",
        Lastname: "",
        Gender: "M",
        Lot: 4,
        Lifts: {
            Squat: [],
            Bench: [
                {
                    Requested: 100,
                    Status: LiftStatus.NOT_ATTEMPTED
                },
                {
                    Requested: 101,
                    Status: LiftStatus.NOT_ATTEMPTED
                }
            ],
            Deadlift: []
        }
    },
    {
        GroupName: "G1",
        Firstname: "",
        Lastname: "",
        Gender: "M",
        Lot: 5,
        Lifts: {
            Squat: [
                {
                    Requested: 100,
                    Status: LiftStatus.SUCCESFUL
                },
                {
                    Requested: 101,
                    Status: LiftStatus.NOT_ATTEMPTED
                }
            ],
            Bench: [],
            Deadlift: []
        }
    }

]
