import SocketServer from "socket.io";
import SocketClient from "socket.io-client";
import { SocketServerDataGroup, SocketServerRoomDataGroup, AuthFunction } from "../src";
import { ReactivePropertyGroup, ReactiveProperty, StreamGroup, Sleep, RoomData } from "shared";
import { Subject } from "rxjs";

class SyncedTest implements ReactivePropertyGroup {
    Age: ReactiveProperty<number> = new ReactiveProperty<number>(0);
    [key: string]: ReactiveProperty<any>;
}

class SyncedRoomTest implements ReactivePropertyGroup {
    Name: ReactiveProperty<string> = new ReactiveProperty<string>("");
    [key: string]: ReactiveProperty<any>;
}
class RoomSubjects implements StreamGroup {
    [key: string]: Subject<any>;
    public AgeEvent: Subject<number> = new Subject<number>();
}
function ParseRoomData<T = any>(data: RoomData<T>) {
    return data.Data;
}
describe("SocketDataGroup", () => {
    const port = 4000;
    const server = SocketServer(port);
    const client = SocketClient(`localhost:${port}`)
    const ServerDataGroup = new SocketServerDataGroup(server, new SyncedTest());

    it("Binds Data through socket", async () => {        
        await Sleep(50);
        ServerDataGroup.PropGroup.Age.Value = 0;
        await Sleep(50);
        ServerDataGroup.PropGroup.Age.Value = 10;
        await Sleep(100);        

    }, 400);
    afterAll(() => {
        client.close();
        server.close();
        ServerDataGroup.Dispose();
    });

});
describe("SocketDataGroupRoom", () => {
    const port = 4003;
    const server = SocketServer(port);
    const client = SocketClient(`localhost:${port}`);
    const client2 = SocketClient(`localhost:${port}`);
    const correctRoom = "1";
    const incorrectRoom = "2";
    const ServerDataGroupRoom = new SocketServerRoomDataGroup(correctRoom, new SyncedRoomTest(), new RoomSubjects(), new RoomSubjects(), server);
    //const ClientDataGroupRoom = new SocketClientRoom(correctRoom, new SyncedRoomTest(), new RoomSubjects(), new RoomSubjects(), client);
    //const IncorrectClientDataGroupRoom = new SocketClientRoom(incorrectRoom, new SyncedRoomTest(), {}, {}, client2);    
    it("Client receives server event", done => {
        client.on("AgeEvent", (v: RoomData) => {
            expect(v.Data).toBe(10);
            done();
        });
        ServerDataGroupRoom.TxStream.AgeEvent.next(10);
    }, 400);
    // it("Server receives client event", done => {
    //     ServerDataGroupRoom.RxStream.AgeEvent.subscribe(v => {
    //         expect(v).toEqual(5);
    //         done();
    //     });
    //     ClientDataGroupRoom.TxStream.AgeEvent.next(5);
    // }, 400);
    // it("Doesn't emit event on bad server data", async () => {
    //     const LocalServer = SocketServer(4002);
    //     const LocalClient = SocketClient("localhost:4002");
    //     const ServerRoom = new SocketServerRoomDataGroup(correctRoom, new SyncedRoomTest(), new RoomSubjects(), new RoomSubjects(), LocalServer);
    //     //const ClientRoom = new SocketClientRoom(correctRoom, new SyncedRoomTest(), new RoomSubjects(), new RoomSubjects(), LocalClient);
    //     const mockFn = jest.fn();
    //     ClientRoom.RxStream.AgeEvent.subscribe(v => mockFn());

    //     await Sleep(50)

    //     ServerRoom.TxStream.AgeEvent.next(15);
    //     LocalServer.emit("AgeEvent", null);

    //     await Sleep(50);

    //     expect(mockFn).toHaveBeenCalledTimes(1);
    //     ServerRoom.TxStream.AgeEvent.next(16);

    //     await Sleep(50);

    //     expect(mockFn).toHaveBeenCalledTimes(2);
    //     LocalClient.close();
    //     LocalServer.close();
    // }, 400);
    // it("Closes connection when token isn't transmitted", async () => {
    //     const LocalServer = SocketServer(4004);
    //     const LocalClient = SocketClient("localhost:4004");

    //     const authFunc: AuthFunction = token => Promise.resolve();
    //     const ServerRoom = new SocketServerRoomDataGroup(correctRoom, new SyncedRoomTest(), new RoomSubjects(), new RoomSubjects(), LocalServer, authFunc);
    //     //const ClientRoom = new SocketClientRoom(correctRoom, new SyncedRoomTest(), new RoomSubjects(), new RoomSubjects(), LocalClient);


    //     await Sleep(1300);
    //     const LocalClient2 = SocketClient("localhost:4004");
    //     expect(LocalClient.disconnected).toBe(true);

    //     //const ClientRoom2 = new SocketClientRoom(correctRoom, new SyncedRoomTest(), new RoomSubjects(), new RoomSubjects(), LocalClient2);
    //     ClientRoom2.TransmitToken("token");
    //     await Sleep(200);

    //     expect(LocalClient2.disconnected).toBe(false);

    //     LocalClient.close();
    //     LocalClient2.close();
    //     LocalServer.close();
    // }, 2000)
    afterAll(() => {
        client.close();
        client2.close();
        server.close();
        ServerDataGroupRoom.Dispose();        
    });

})