import { DataGroupRoom, PlatformData, ServerRxPlatformStream, ServerTxPlatformStream, Sleep } from "shared";
import { InitEmptyLokiDB } from "../src/database/loki";
import { SessionManager, PlatformSocketFactoryFn, SocketServerRoomDataGroup } from "../src";
import { LiftOrderData } from "./test-data/athletes";
import SocketServer from "socket.io";
const server = SocketServer(4010);
class MockClass extends SocketServerRoomDataGroup<PlatformData, ServerRxPlatformStream, ServerTxPlatformStream>{
    constructor(Room: string, PlatformData: PlatformData, ServerRxPlatformStream: ServerRxPlatformStream, ServerTxPlatformStream: ServerTxPlatformStream) {
        super(Room, PlatformData, ServerRxPlatformStream, ServerTxPlatformStream, server);
    }
}
describe("Session Manager", () => {
    it("Creates sessions from database", async () => {
        const db = InitEmptyLokiDB();

        const platformData = new PlatformData()
        const serverRxPlatformStream = new ServerRxPlatformStream();
        const serverTxPlatformStream = new ServerTxPlatformStream();
        const platformSocketFactoryFn: PlatformSocketFactoryFn = Room => new MockClass(Room, platformData, serverRxPlatformStream, serverTxPlatformStream);
        const mngr = new SessionManager(db, platformSocketFactoryFn);
        const inserted = db.Platforms.Collection.insertOne({ Name: "TEST" });
        db.Groups.Collection.insertOne({ Name: "G1", PlatformName: "TEST" });
        db.Athletes.Collection.insert(LiftOrderData);
        db.Platforms.Collection.remove(inserted.$loki);

        await Sleep(50);
        expect(mngr.Sessions.length).toBe(1);
        serverRxPlatformStream.SetActiveGroupName.next("G1");
        await Sleep(50);
    }, 200);
    afterAll(() => server.close());
});