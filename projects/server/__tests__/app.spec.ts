import { InitLoki, InitializeApplication, LokiDB, InitEmptyLokiDB } from "../src";
import request from "supertest";
import { HttpJsonResponse, AthleteData, Athlete, LOKI, LiftStatus } from "shared";
describe("Application", () => {
    let app = InitializeApplication(InitEmptyLokiDB());
    let AGENT = request.agent(app);
    beforeEach(() => {
        app = InitializeApplication(InitEmptyLokiDB());
        AGENT = request.agent(app);
    })
    it("Creates", async () => {

        expect(app).not.toBeNull();
    });

    it("Adds entries", async () => {
        const res = (await AGENT.post("/api/athlete").send(mockData).expect(200)).body as HttpJsonResponse<LOKI<Athlete>>;
        const data = res.Data;
        expect(res.Status).toBe("SUCCESS");
        expect(res.Data).toBeTruthy();
        expect(data.$loki).toBe(1);
        await AGENT.post("/api/athlete").send(mockData).expect(200);
        const res2 = (await AGENT.get("/api/athlete").expect(200)).body as HttpJsonResponse<LOKI<Athlete>[]>;
        expect(res2.Status).toBe("SUCCESS");
        expect(res2.Data.length).toBe(2);
    });
    it("Reads single entry", async () => {
        await AGENT.post("/api/athlete").send(mockData).expect(200);
        await AGENT.post("/api/athlete").send({ ...mockData, Gender: "F" }).expect(200);
        expect((await AGENT.get("/api/athlete").expect(200)).body.Data.length).toBe(2);
        const res = (await AGENT.get(`/api/athlete/${2}`).expect(200)).body.Data as LOKI<Athlete>;
        expect(res.Gender).toBe("F");
    });
    it("Updates complete entry", async () => {
        let res = (await AGENT.post("/api/athlete").send(mockData).expect(200)).body as HttpJsonResponse<LOKI<Athlete>>;
        const data = res.Data;
        expect(data.Firstname).toBe(mockData.Firstname);
        data.Firstname = "Jane";
        res = (await AGENT.put(`/api/athlete`).send(data).expect(200)).body as HttpJsonResponse<LOKI<Athlete>>
        expect(res.Status).toBe("SUCCESS");
        expect(res.Data.Firstname).toBe("Jane");
    });
    it("Patches partial entry", async () => {
        let res = (await AGENT.post("/api/athlete").send(mockData).expect(200)).body as HttpJsonResponse<LOKI<Athlete>>;
        const data = res.Data;
        expect(data.Firstname).toBe(mockData.Firstname);

        res = (await AGENT.patch(`/api/athlete/${data.$loki}`).send({ Firstname: "Jane", Gender: "F" } as Partial<Athlete>).expect(200)).body as HttpJsonResponse<LOKI<Athlete>>
        expect(res.Status).toBe("SUCCESS");
        expect(res.Data.Firstname).toBe("Jane");
        expect(res.Data.Gender).toBe("F");
    });
    it("Errors on delete non-existing entry", async () => {
        const res = (await AGENT.delete(`/api/athlete/${1}`).expect(200)).body as HttpJsonResponse<null>;
        expect(res.Status).toBe("ERROR");
        expect(res.Data).toBeNull();
    });
    it("Deletes existing entry", async () => {
        let res = (await AGENT.post("/api/athlete").send(mockData).expect(200)).body as HttpJsonResponse<LOKI<Athlete>>;
        res = (await AGENT.delete(`/api/athlete/${1}`).expect(200)).body as HttpJsonResponse<null>;        
        expect(res.Status).toBe("SUCCESS");
        expect(res.Data).toBeNull();
    });
});
const mockData: Athlete = {
    Firstname: "John",
    Lastname: "Doe",
    Gender: "M",
    Lot: 1,
    Lifts: {
        Squat: [],
        Bench: [
            {
                Requested: 101,
                Status: LiftStatus.NOT_ATTEMPTED
            },
            {
                Requested: 102,
                Status: LiftStatus.NOT_ATTEMPTED
            },
            ,
            {
                Requested: 102,
                Status: LiftStatus.NOT_ATTEMPTED
            }],
        Deadlift: []
    }
}
