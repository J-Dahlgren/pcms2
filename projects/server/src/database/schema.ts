import Lowdb = require("lowdb");
import { Athlete, AthleteData, Entry } from "shared";

export interface PcmsSchema {
    athletes: Entry<AthleteData>[];
}