import * as loki from "lokijs";
import { BehaviorSubject, Subject } from "rxjs";
import { LOKI, Athlete, Group, Platform } from "shared";
import { singleton } from "tsyringe";
import { Log } from "../logging";
export function InitLoki(fileName: string, persist: boolean = true) {
    return new Promise<Loki>((resolve, reject) => {
        const autoloadCallback = (err?: any) => {
            if (err) {
                reject(err);
            } else {
                resolve(db);
            }
        };
        const db: loki = new loki.default(fileName, {
            autoload: persist,
            autosave: persist,
            autosaveInterval: persist ? 5000 : undefined,
            autoloadCallback: persist ? autoloadCallback : undefined
        });
        if (!persist) {
            resolve(db);
        }
    });
}
export function InitEmptyLokiDB() {
    const db = new loki.default("db");
    return new LokiDB(db);
}
export class LokiCollection<T extends object>{
    protected datastream: BehaviorSubject<LOKI<T>[]> = new BehaviorSubject<LOKI<T>[]>([]);
    protected deleteEvent: Subject<number> = new Subject<number>();
    protected addEvent: Subject<LOKI<T>> = new Subject<LOKI<T>>();
    protected updateEvent: Subject<LOKI<T>> = new Subject<LOKI<T>>();
    private BlockEvents = false;

    public get Stream() {
        return this.datastream.asObservable();
    }
    public get DeleteEvent() {
        return this.deleteEvent.asObservable();
    }
    public get AddEvent() {
        return this.addEvent.asObservable();
    }
    public get UpdateEvent() {
        return this.updateEvent.asObservable();
    }
    constructor(protected db: loki, protected collectionName: string) {
        this.Collection.on(["insert", "update", "delete"], () => {
            if (!this.BlockEvents) this.datastream.next(this.data);
        });
        this.Collection.on(["delete"], (RemovedElements: LOKI<T> | LOKI<T>[]) => {
            if (!this.BlockEvents) {
                if (Array.isArray(RemovedElements)) {
                    for (const removed of RemovedElements) {
                        this.deleteEvent.next(removed.$loki);
                    }
                } else {
                    this.deleteEvent.next(RemovedElements.$loki);
                }
            }
        });
        this.Collection.on(["insert"], (AddedElements: LOKI<T> | LOKI<T>[]) => {
            if (!this.BlockEvents) {
                if (Array.isArray(AddedElements)) {
                    for (const added of AddedElements) {
                        this.addEvent.next(added);
                    }
                } else {
                    this.addEvent.next(AddedElements);
                }
            }
        });
        this.Collection.on(["update"], (UpdatedElements: LOKI<T> | LOKI<T>[]) => {
            if (!this.BlockEvents) {
                if (Array.isArray(UpdatedElements)) {
                    for (const updated of UpdatedElements) {
                        this.updateEvent.next(updated);
                    }
                } else {
                    this.updateEvent.next(UpdatedElements);
                }
            }
        });
        this.datastream.next(this.data);
    }

    public get Collection() {
        const collection: Collection<T> | null = this.db.getCollection<T>(this.collectionName);
        if (!collection) {
            return this.db.addCollection<T>(this.collectionName, { disableMeta: true });
        }
        return collection;
    }
    public get data() {
        return this.Collection.data as LOKI<T>[];
    }

    /**
     * Blocks any reported changes to the stream until the action is c ompleted, pushes the collection after finished
     * @param CB Action to be performed
     */
    public PerformBlockingAction(CB: () => void) {
        this.BlockEvents = true;
        try {
            CB();
        } catch (error) {

        }
        this.BlockEvents = false;
        this.datastream.next(this.data); // Make sure any changes are pushed to clients
    }
}
export class PlatformCollection extends LokiCollection<Platform>{
    constructor(db: loki, collectionName: string) {
        super(db, collectionName);
        if (this.data.length === 0) {
            Log.info("No platform available, creating a default");
            this.Collection.insertOne({ Name: "Main" });

        }
    }
}
export class LokiDB {
    public readonly Athletes: LokiCollection<Athlete>;
    public readonly Groups: LokiCollection<Group>;
    public readonly Platforms: LokiCollection<Platform>;
    constructor(private db: loki) {
        this.Athletes = new LokiCollection<Athlete>(db, "athletes");
        this.Groups = new LokiCollection<Group>(db, "groups");
        this.Platforms = new PlatformCollection(db, "platforms");


        // TODO: Validate groups and platforms
    }
}