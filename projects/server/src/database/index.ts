import LowDb from "lowdb";

import { PcmsSchema } from "./schema";
export * from "./loki";
export * from "./schema";
export function InitDatabase(adapter: LowDb.AdapterAsync<PcmsSchema>) {
    return LowDb(adapter);
}
