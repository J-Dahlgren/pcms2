import { SocketServerRoomDataGroup } from "./ServerDataGroup";
import { PlatformData, ServerRxPlatformStream, ServerTxPlatformStream } from "shared";

export type PlatformSocketFactoryFn = (Room: string) => SocketServerRoomDataGroup<PlatformData, ServerRxPlatformStream, ServerTxPlatformStream>;