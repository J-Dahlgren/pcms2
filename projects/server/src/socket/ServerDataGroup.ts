import { ReactivePropertyGroup, DataGroup, CLIENT_SYNC_REQUEST, StreamGroup, DataGroupRoom, RoomData, CLIENT_AUTH_REQUEST, IDisposable } from "shared";
import * as io from "socket.io";
import { Subject, Observable, Subscription } from "rxjs";
import { setTimeout } from "timers";
export type AuthFunction = (token: string) => Promise<void>;
export class SocketServerDataGroup<T extends ReactivePropertyGroup> extends DataGroup<T> implements IDisposable {
    constructor(server: io.Server, syncData: T) {
        super(syncData, {}, {});
        this.Subs.push(this.syncRequest.subscribe(client => this.SyncRequest(client)));
        server.on("connection", client => {
            client.on(CLIENT_SYNC_REQUEST, () => this.syncRequest.next(client));
            this.SyncRequest(client);
        });
        for (const key in syncData) {
            this.Subs.push(this.PropGroup[key].Stream.subscribe((v: any) => server.emit(key, v)));
        }
    }
    protected Subs: Subscription[] = [];
    Dispose() {
        this.Subs.forEach(sub => sub.unsubscribe());
        this.Subs = [];
    }
    protected syncRequest = new Subject<io.Socket>();
    public get OnSyncRequest(): Observable<io.Socket> {
        return this.syncRequest.asObservable();
    }
    protected SyncRequest(client: io.Socket) {
        for (const key in this.PropGroup) {
            client.emit(key, this.PropGroup[key].Value);
        }
    }
}
export class SocketServerRoomDataGroup<RPG extends ReactivePropertyGroup, RxStream extends StreamGroup, TxStream extends StreamGroup> extends DataGroupRoom<RPG, RxStream, TxStream> implements IDisposable {
    protected Subs: Subscription[] = [];
    Dispose() {
        this.Subs.forEach(sub => sub.unsubscribe());
        this.Subs = [];
    }
    constructor(room: string, syncData: RPG, rxStream: RxStream, txStream: TxStream, server: io.Server, authFunc?: AuthFunction) {
        super(room, syncData, rxStream, txStream);
        this.Subs.push(this.syncRequest.subscribe(client => this.SyncRequest(client)));
        server.on("connection", client => {
            // client.on(JOIN, r => {
            //     client.join(this.Room)
            //     if (r === this.Room) {
            //         this.SyncRequest(client);
            //     }
            // });
            // client.on(LEAVE, r => {
            //     if (r === this.Room) {
            //         client.leave(room);
            //     }
            // })

            // Set timeout to close connection;
            const timer = setTimeout(() => client.disconnect(), 1000);
            const listenFunc = () => {
                // Remove timeout
                clearTimeout(timer);
                // Start listen to client commands
                for (const key in rxStream) {
                    client.on(key, (roomData?: RoomData) => {
                        if (roomData && roomData.Room === this.Room) {
                            rxStream[key].next(roomData.Data);
                        }
                    })
                }
                client.on(CLIENT_SYNC_REQUEST, () => this.syncRequest.next(client));
                this.SyncRequest(client);
            }
            if (typeof authFunc === "function") {
                client.on(CLIENT_AUTH_REQUEST, token => {
                    authFunc(token)
                        .then(() => {
                            listenFunc();
                        })
                        .catch(() => { console.log("Auth failed"); }); // Can be empty since it will be cleared anyway
                });
            } else {
                listenFunc();
            }
        })

        for (const key in syncData) {
            this.Subs.push(this.PropGroup[key].Stream.subscribe((v: any) => server.emit(key, this.CreateRoomData(v))));
        }
        for (const key in txStream) {
            this.Subs.push(txStream[key].subscribe(v => server.emit(key, this.CreateRoomData(v))));
        }
    }
    protected syncRequest = new Subject<io.Socket>();
    public get OnSyncRequest(): Observable<io.Socket> {
        return this.syncRequest.asObservable();
    }
    
    public EmitEvent<T>(socket: io.Socket, eventName: Extract<keyof TxStream, string>, value: T) {
        socket.emit(eventName, this.CreateRoomData(value));
    }
    protected SyncRequest(client: io.Socket) {
        for (const key in this.PropGroup) {
            client.emit(key, this.CreateRoomData(this.PropGroup[key].Value));
        }
    }
}