import express from "express";
import cors from "cors";
import BodyParser from "body-parser";
import { LokiDB } from "./database";
import { CreateRouter } from "./routes";
import { AthleteController } from "./controllers/athlete.controller";
import { GroupController, PlatformController } from "./controllers";
import { Log } from "./logging/Logger";

/**
 * Sets up the main application routes
 * @param db Database object the application interfaces with
 */
export function InitializeApplication(db: LokiDB) {
    const App = express();
    App.use(cors());    
    App.use(BodyParser.urlencoded({ extended: true }));
    App.use((req, res, next) => {
        BodyParser.json()(req, res, err => {
            if (err) {
                Log.error(err);
                return res.sendStatus(400); // Bad request
            }
            return next();
        });
    });
    const APIRouter = express.Router();
    APIRouter.use("/athlete", CreateRouter({ Name: "Athlete", Create: {}, Read: {}, Update: {}, Delete: {} }, new AthleteController(db.Athletes)))
    APIRouter.use("/group", CreateRouter({ Name: "Group", Create: {}, Read: {}, Update: {}, Delete: {} }, new GroupController(db.Groups, db.Athletes)));
    APIRouter.use("/platform", CreateRouter({ Name: "Platform", Create: {}, Read: {}, Update: {}, Delete: {} }, new PlatformController(db.Platforms, db.Groups)));

    //App.use("/upload", <ROUTER>)
    //App.use("/download", <ROUTER>)
    App.use("/api", APIRouter);
    return App;
}
