import { IController } from "./IController";
import { LOKI, AthleteData } from "shared";
import { Athlete } from "shared";
import { AbstractController } from "./abstract.controller";
import { LokiCollection } from "../database";

export class AthleteController extends AbstractController<Athlete>{
    constructor(LokiCol: LokiCollection<Athlete>) {
        super(LokiCol);
    }      
}