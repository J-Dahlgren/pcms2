import { IController } from "./IController";
import { LOKI } from "shared";
import { LokiCollection } from "../database/loki";
export type OperationErrorResult = undefined | false | string;
export interface OperationConstraints<T> {
    CanDelete?: (Entry: LOKI<T>) => OperationErrorResult;
}
export abstract class AbstractController<T extends object> implements IController<T>{
    constructor(protected LokiCol: LokiCollection<T>, protected Constraints: OperationConstraints<T> = {}) {

    }
    Create(Obj: T): LOKI<T> | undefined {
        return this.LokiCol.Collection.insertOne(Obj);
    }
    Read(Id: number): T | undefined {
        return this.LokiCol.Collection.get(Id);
    }
    ReadAll(): LOKI<T>[] {
        return this.LokiCol.data;
    }
    Update(Obj: LOKI<T>): LOKI<T> | undefined {
        const entry = this.LokiCol.Collection.get(Obj.$loki);
        if (entry) {
            Object.assign(entry, Obj);
            this.LokiCol.Collection.update(entry);
            return entry;
        }
        return undefined;
    }
    Patch(Id: number, Obj: Partial<T>): LOKI<T> | undefined {
        const entry = this.LokiCol.Collection.get(Id);
        if (entry) {
            const upd = { ...entry, ...Obj } as LOKI<T>;
            return this.Update(upd);
        }
        return undefined;
    }
    Delete(Id: number): boolean {

        const entry = this.LokiCol.Collection.get(Id);
        if (!entry) {
            return false;
        }
        if (this.Constraints.CanDelete) {
            const err = this.Constraints.CanDelete(entry);
            if (err) throw new Error(err);
        }
        return !!this.LokiCol.Collection.remove(Id);
    }
}