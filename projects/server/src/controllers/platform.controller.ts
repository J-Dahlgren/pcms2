import { IController } from "./IController";
import { LOKI, AthleteData, Athlete, Platform } from "shared";
import { Group } from "shared";
import { AbstractController } from "./abstract.controller";
import { LokiCollection } from "../database";

export class PlatformController extends AbstractController<Platform>{
    constructor(Platforms: LokiCollection<Platform>, protected Groups: LokiCollection<Group>) {
        super(Platforms);
        this.Constraints = {
            CanDelete: entry => {
                if (this.LokiCol.data.length <= 1) {
                    return `Not allowed to delete last platform`;
                }
                if (this.Groups.Collection.findOne({ PlatformName: { $eq: entry.Name } })) {
                    return `Can't delete platform ${entry.Name} when a group is assigned to it`;
                }
                return false;
            }
        }
    }
}