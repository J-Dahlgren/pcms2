import { LOKI } from "shared";

export interface IController<T extends object> {
    Create: (Obj: T) => LOKI<T> | undefined;
    Read: (Id: number) => T | undefined;
    ReadAll: () => LOKI<T>[];
    Update: (Obj: LOKI<T>) => LOKI<T> | undefined;
    Patch: (Id: number, Obj: Partial<T>) => LOKI<T> | undefined;
    Delete: (Id: number) => boolean;
}