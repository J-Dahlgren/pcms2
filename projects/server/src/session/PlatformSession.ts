import { PlatformSocket, PlatformDataGroup } from "../socket";
import { LokiDB } from "../database";
import { Subscription } from "rxjs";
import { map, filter, distinctUntilChanged } from "rxjs/operators";
import { IDisposable, LOKI, Athlete, LiftingOrderFn, LiftingOrder, GetAttemptInfo, GetCurrentAttempt, CountdownTimer } from "shared";
import { Log } from "../logging/Logger";
import { Socket } from "socket.io";
import { isEqual, cloneDeep } from "lodash";
interface Locker {
    /**
     * Time in millis
     */
    TimeRemaining: number;
    Athlete: LOKI<Athlete> | null;
    Force?: boolean;
}
/**
 * This class contains the all the logic and states for a platform. Its states are not persisted.
 */
export class PlatformSession implements IDisposable {

    protected get DataGroup() {
        return this.PlatformSocket.PropGroup;
    }
    protected get RxEvents() {
        return this.PlatformSocket.RxStream;
    }
    protected Clock = new CountdownTimer(100);
    protected Subs: Subscription[] = [];
    protected Locker: Locker = { Athlete: null, TimeRemaining: 0 };
    constructor(public readonly Id: number, protected PlatformSocket: PlatformDataGroup, protected readonly db: LokiDB) {
        this.Subs = [
            // Update Event for this platform   
            db.Platforms.UpdateEvent.pipe(filter(x => x.$loki === this.Id)).subscribe(platform => {
                
            }),
            this.RxEvents.SetActiveGroupName.subscribe(GroupName => this.DataGroup.ActiveGroupName.Value = GroupName),
            this.DataGroup.ActiveGroupName.Stream.subscribe(ActiveGroupName => {
                // ActiveGroupName must be truthy and match the athletes group
                this.DataGroup.ActiveAthletes.Value = db.Athletes.data.filter(a => ActiveGroupName && a.GroupName === ActiveGroupName);
            }),
            this.PlatformSocket.OnSyncRequest.subscribe((socket: Socket) => {
                // This is necessary since we don't want to update the client whenever the clock changes since that occurs many times every second
                this.PlatformSocket.EmitEvent(socket, "ClockState", this.Clock.State);
            }),
            this.Clock.Status.Stream.pipe(distinctUntilChanged()).subscribe(status => {
                if (status === "RUNNING") {
                    // The athlete is locked when the clock starts
                    this.Locker.Athlete = cloneDeep(this.DataGroup.CurrentAthlete.Value);
                    // The time is also preserved
                    this.Locker.TimeRemaining = this.Clock.RemainingMillis.Value;
                }
            }),
            db.Athletes.Stream
                .pipe(map(athletes => athletes.filter(a => this.DataGroup.ActiveGroupName.Value && a.GroupName === this.DataGroup.ActiveGroupName.Value))) // ActiveGroupName must be truthy and match the athletes group
                .subscribe(filtered => this.DataGroup.ActiveAthletes.Value = LiftingOrder(filtered)),
            this.DataGroup.ActiveAthletes.Stream.subscribe(activeAthletes => {

                if (activeAthletes.length >= 1) {
                    // Assign current athlete
                    const currentInfo = GetAttemptInfo(activeAthletes[0].Lifts);
                    this.DataGroup.CurrentAthlete.Value = currentInfo.LiftName !== "Done" ? activeAthletes[0] : null;
                }
                if (activeAthletes.length >= 2) {
                    // Assign next athlete
                    const nextInfo = GetAttemptInfo(activeAthletes[1].Lifts);
                    this.DataGroup.NextAthlete.Value = nextInfo.LiftName !== "Done" ? activeAthletes[1] : null;
                }
                if (activeAthletes.length === 0) {
                    Log.warn("No lifters in group");
                }
            }),
            this.DataGroup.CurrentAthlete.Stream
                .pipe(distinctUntilChanged((a, b) => isEqual(a, b)))
                .subscribe(athlete => {
                    if (!athlete) {
                        Log.info("There is no current athlete");
                    } else {
                        const info = GetAttemptInfo(athlete.Lifts);
                        Log.info(`Current athlete is: ${athlete.Firstname} ${athlete.Lastname} | ${info.LiftName} ${info.AttemptNumberOneIndexed}: ${info.Weight} kg`);
                        // Compare the new current athlete with the old value to determine what should happen to the clock
                    }
                })
        ];
    }

    Dispose() {
        this.Subs.forEach(sub => sub.unsubscribe());
        this.Subs = [];
    }
}