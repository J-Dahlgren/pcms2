import { PlatformSession } from "./PlatformSession";
import { LokiDB } from "../database";
import { PlatformSocketFactoryFn } from "../socket";

export class SessionManager {
    Sessions: PlatformSession[];
    constructor(db: LokiDB, platformSocketFactoryFn: PlatformSocketFactoryFn,) {
        this.Sessions = [];
        db.Platforms.data.forEach(p => this.Sessions.push(new PlatformSession(p.$loki, platformSocketFactoryFn(p.$loki.toString()), db)));
        db.Platforms.AddEvent.subscribe(p => this.Sessions.push(new PlatformSession(p.$loki, platformSocketFactoryFn(p.$loki.toString()), db)));
        db.Platforms.DeleteEvent.subscribe(pId => {
            const index = this.Sessions.findIndex(e => e.Id === pId);
            if (index > -1) {
                const Session = this.Sessions[index];
                Session.Dispose();
                this.Sessions.splice(index, 1);
            }
        });

    }
}