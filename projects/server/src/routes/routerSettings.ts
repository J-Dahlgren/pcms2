export type SchemaValidator = {
    Schema?: any
};
export interface RouterSettings {
    Name: string;
    Create?: SchemaValidator;
    Read?: SchemaValidator;
    Update?: SchemaValidator;    
    Delete?: SchemaValidator;
}
