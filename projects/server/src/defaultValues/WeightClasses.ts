import { WeightClass, Gender } from "shared";
const MensClasses: WeightClass[] = [
    {
        Gender: "M",
        MinExlusive: 0,
        MaxInclusive: 53,
        AgeRestriction: ["Sub-junior", "Junior"]
    },
    {
        Gender: "M",
        MinExlusive: 0,
        MaxInclusive: 59,
    },
    {
        Gender: "M",
        MinExlusive: 59,
        MaxInclusive: 66,
    },
    {
        Gender: "M",
        MinExlusive: 66,
        MaxInclusive: 74,
    },
    {
        Gender: "M",
        MinExlusive: 74,
        MaxInclusive: 83,
    },
    {
        Gender: "M",
        MinExlusive: 83,
        MaxInclusive: 93,
    },
    {
        Gender: "M",
        MinExlusive: 93,
        MaxInclusive: 105,
    },
    {
        Gender: "M",
        MinExlusive: 105,
        MaxInclusive: 120,
    },
    {
        Gender: "M",
        Name:"120+",
        MinExlusive: 120,
        MaxInclusive: Number.MAX_SAFE_INTEGER,
    },
];
export const WeightClasses: { [key in Gender]: WeightClass[] } = {
    M:MensClasses,
    F:[]
}