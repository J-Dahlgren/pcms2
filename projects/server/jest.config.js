module.exports = {
    preset: "ts-jest",
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.ts$",
    testPathIgnorePatterns: [
        "test-data"
    ],
    globals: {
        "ts-jest": {
            tsConfig: "projects/server/tsconfig.json",
            diagnostics: false
        }
    }
}
