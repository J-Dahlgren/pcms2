export type AgeCategoryName = "Sub-junior" | "Junior" | "Masters 1" | "Masters 2" | "Masters 3" | "Masters 4";

export interface AgeCategory {
    MaxAge: number;
    MinAge: number;
}
export const AgeCategories: { [key in AgeCategoryName]: AgeCategory } = {
    "Sub-junior": {
        MinAge: 14,
        MaxAge: 18
    },
    Junior: {
        MinAge: 19,
        MaxAge: 23,
    },
    "Masters 1": {
        MinAge: 35,
        MaxAge: 40
    },
    "Masters 2": {
        MinAge: 35,
        MaxAge: 40
    },
    "Masters 3": {
        MinAge: 35,
        MaxAge: 40
    },
    "Masters 4": {
        MinAge: 35,
        MaxAge: 40
    }
}
// export function GetAgeCategory(athlete:Athlete, )
export type AgeCoefficients = "None" | "FosterMcCulloch";