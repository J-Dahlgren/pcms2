export type Id = string | number;
export type Entry<T extends object> = T & { id: Id };
