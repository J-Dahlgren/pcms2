import { Nullable } from "../Util/Nullable";
export type Mode = "SBD" | "B";
export enum CompetitionModes {
    CLASSIC = "CLASSIC",
    BENCH = "BENCH_PRESS"
}
export type Discipline = {
    [index: string]: number;
    Squat: 0;
    Bench: 1;
    Deadlift: 2;
    Done: 4;
}
export const DISCIPLINE: Discipline = {
    Squat: 0,
    Bench: 1,
    Deadlift: 2,
    Done: 4,
}
export const Disc: {
    [key in LiftFieldExt]: number
} = {
    Squat: 0,
    Bench: 1,
    Deadlift: 2,
    Done: 0
}
export enum LiftStatus {
    FAILED_NOT_TAKEN = -2,
    FAILED = -1,
    NOT_ATTEMPTED = 0,
    SUCCESFUL = 1
}
export interface Attempt {
    Status: LiftStatus;
    Automatic?: Nullable<number>;
    Requested?: Nullable<number>;
}
export type LiftField = "Squat" | "Bench" | "Deadlift";
export type LiftFieldExt = LiftField | "Done";
export const LiftFieldTuple: LiftField[] = ["Squat", "Bench", "Deadlift"];
export type Lifts = {
    [key in LiftField]: Attempt[];
}
export interface LiftInfo {
    Name: keyof Lifts | LiftField,
    Attempt: Attempt,
    AttemptNumberOneIndexed: number,
    RequestedWeight: number
}
export function GetCurrentAttempt(lifts: Lifts): Nullable<LiftInfo> {

    for (const Field of LiftFieldTuple) { // Enforce order of keys        
        const arr = lifts[Field];
        for (let i = 0; i < arr.length; i++) {
            const Attempt = arr[i];
            const weight = GetAttemptWeight(Attempt);
            if (Attempt.Status === LiftStatus.NOT_ATTEMPTED && weight) {
                return {
                    Name: Field,
                    Attempt,
                    AttemptNumberOneIndexed: i + 1,
                    RequestedWeight: weight
                }
            }
        }
    }
    return null;
}

export interface AttemptInfo {
    LiftName: LiftFieldExt,
    Discipline: number,
    AttemptNumberOneIndexed: number,
    Weight: number
}
export function GetAttemptInfo(lifts: Lifts): AttemptInfo {
    const info = GetCurrentAttempt(lifts);
    return {
        LiftName: info ? info.Name : "Done",
        Discipline: info ? DISCIPLINE[info.Name] : DISCIPLINE.Done,
        AttemptNumberOneIndexed: info ? info.AttemptNumberOneIndexed : Number.MAX_SAFE_INTEGER,
        Weight: info ? info.RequestedWeight : Number.MAX_SAFE_INTEGER,
    }
}
export function GetAttemptWeight(attempt: Attempt): Nullable<number> {
    return attempt.Requested || attempt.Automatic;
}
