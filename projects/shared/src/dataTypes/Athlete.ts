
import { Lifts } from "./Lift";
import { Entry } from "./Entry";
import { Group } from "./Competition";
export type Gender = "M" | "F";
export type SquatRackTiltType = "NONE" | "BOTH" | "LEFT" | "RIGHT";
export const SquatRackTilt: { [key in SquatRackTiltType]: number } = {
    NONE: 0,
    BOTH: 1,
    LEFT: 2,
    RIGHT: 3
}
export interface BenchPressSettings {
    RackHeight: number;
    SafetyRackHeight: number;
    LiftOffAssistance?: boolean;
}
export interface SquatRackSettings {
    RackSetting: SquatRackTiltType;
    RackHeight: number;
}
export interface AthleteData {
    Lot: number;
    Firstname: string;
    Lastname: String;
    License?: string;
    BornYear?: number;
    Gender: Gender,
    Team?: string;
}
export interface Athlete extends AthleteData {
    Bodyweight?: number;
    GroupName?: string;
    SquatRackInfo?: SquatRackSettings;
    BenchRackInfo?: BenchPressSettings;
    Lifts: Lifts;
}
export type AthleteEntry = Entry<Athlete>;

export type AgeCategoryName = "Sub-junior" | "Junior" | "Masters 1" | "Masters 2" | "Masters 3" | "Masters 4" | "Open";

export interface AgeCategory {
    Name: AgeCategoryName;
    MaxAge: number;
    MinAge: number;
}

export const AgeCategories: { [key in AgeCategoryName]: AgeCategory; } = {
    "Sub-junior": {
        Name: "Sub-junior",
        MinAge: 14,
        MaxAge: 18
    },
    Junior: {
        Name: "Junior",
        MinAge: 19,
        MaxAge: 23,
    },
    "Masters 1": {
        Name: "Masters 1",
        MinAge: 40,
        MaxAge: 49
    },
    "Masters 2": {
        Name: "Masters 2",
        MinAge: 50,
        MaxAge: 59
    },
    "Masters 3": {
        Name: "Masters 3",
        MinAge: 60,
        MaxAge: 69
    },
    "Masters 4": {
        Name: "Masters 3",
        MinAge: 70,
        MaxAge: Number.MAX_SAFE_INTEGER
    },
    Open: {
        Name: "Open",
        MinAge: 14,
        MaxAge: Number.MAX_SAFE_INTEGER
    }

}

export function GetAgeCategory(athlete: AthleteData): AgeCategory {
    const born = athlete.BornYear || new Date().getFullYear();
    const ageYear = new Date().getFullYear() - born;
    for (const catStr in AgeCategories) {
        const cat = AgeCategories[catStr as AgeCategoryName];
        if (cat.MinAge <= ageYear && ageYear <= cat.MaxAge) {
            return cat;
        }
    }
    return AgeCategories.Open; // Default
}
export type AgeCoefficients = "None" | "FosterMcCulloch";

export interface WeightClass {
    MinExlusive: number;
    MaxInclusive: number;
    Gender: Gender;
    Name?: string;
    AgeRestriction?: AgeCategoryName[];
}
export function GetWeightClass(athlete: Athlete, classes: { [key in Gender]: WeightClass[] }): WeightClass | null {
    const bw = athlete.Bodyweight || 0;
    for (const c of classes[athlete.Gender]) {
        const ageCat = GetAgeCategory(athlete);
        if (c.AgeRestriction) {
            if (c.AgeRestriction.indexOf(ageCat.Name) < 0) {
                continue; // Athletes age category isn't included in the list of restrictions
            }
        }
        if (c.MinExlusive < bw && bw <= c.MaxInclusive) {
            return c;
        }
    }
    return null;
}
