import { BehaviorSubject } from "rxjs";
import { distinctUntilChanged } from "rxjs/operators";
import { ReactiveProperty } from "../Util";

export type ClockStates = "PAUSED" | "RUNNING";
export interface ClockState {
    Status: ClockStates;
    RemainingMillis?: number;
}

export class CountdownTimer {    
    public readonly Status = new ReactiveProperty<ClockStates>("PAUSED");
    public readonly RemainingMillis = new ReactiveProperty<number>(0);
    private Interval = setTimeout(() => { }, 0);   // Initialize with empty timer

    protected endTime: number = 0;
    constructor(private TickInvervalMillis: number = 100) {
        this.RemainingMillis.Stream.subscribe(t => {
            if (t < 0) {
                this.RemainingMillis.Value = 0;
                this.Status.Value = "PAUSED";
            }
        });
        this.Status.Stream.pipe(distinctUntilChanged()).subscribe(Status => {
            if (Status === "RUNNING") {
                this.endTime = Date.now() + this.RemainingMillis.Value;
                this.SetInterval();
            } else {
                this.ClearInterval();
            }
        });

    }
    public get State(): ClockState {
        return { Status: this.Status.Value, RemainingMillis: this.RemainingMillis.Value };
    }
    public Pause(RemainingMillis?: number) {
        this.Status.Value = "PAUSED";
        if (typeof RemainingMillis === "number" && RemainingMillis >= 0) {
            this.RemainingMillis.Value = RemainingMillis;
        }
    }
    public Start() {
        this.Status.Value = "RUNNING";
    }
    public SetState(state: ClockState) {
        if (state.RemainingMillis) {
            this.RemainingMillis.Value = state.RemainingMillis;
        }
        this.Status.Value = state.Status;
    }
    protected Tick() {
        if (this.Status.Value === "RUNNING") {
            const t = this.remainingMillis;
            if (t <= 0) {
                this.Status.Value = "PAUSED";
            }
            this.RemainingMillis.Value = t;
        }
    }
    protected SetInterval() {
        this.ClearInterval();
        this.Interval = setInterval(() => this.Tick(), this.TickInvervalMillis);
        this.Tick(); // run function once at first to avoid delay        
    }
    protected ClearInterval() {
        try {
            clearInterval(this.Interval);
        } catch (e) {

        }
    }

    protected get remainingMillis(): number {
        if (this.Status.Value === "PAUSED") {
            return this.RemainingMillis.Value;
        }
        const delta = this.endTime - Date.now();
        return delta >= 0 ? delta : 0;
    }
}
