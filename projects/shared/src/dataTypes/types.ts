import { Id } from "./Entry";

export interface ICompetition {
    name: string;
    platforms: IPlatform[];
}
export interface IPlatform {
    name: string;
    competitionId: Id;
}
