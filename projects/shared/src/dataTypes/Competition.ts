export interface Platform {
    Name: string;
}
export interface Group {
    Name: string;
    PlatformName?: string;
}