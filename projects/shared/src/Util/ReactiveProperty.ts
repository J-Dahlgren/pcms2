import { BehaviorSubject, Observable } from "rxjs";
export class ReactiveProperty<T = any>{
    protected value: BehaviorSubject<T>;
    constructor(InitialValue: T) {
        this.value = new BehaviorSubject<T>(InitialValue);
    }
    public get Value(): T {
        return this.value.value;
    }
    public set Value(v: T) {
        this.value.next(v);
    }
    public get Stream(): Observable<T> {
        return this.value.asObservable();
    }
}
export class NullReactiveProperty<T> extends ReactiveProperty<T | null>{
    constructor(InitialValue: (T | null) = null) {
        super(InitialValue);
    }
}
export class ReactiveList<T> extends ReactiveProperty<T[]> {
    constructor(InitialElements: T[] = []) {
        super(InitialElements);
    }
}