import { ReactivePropertyGroup, StreamGroup } from "./DataGroup";
import { ReactiveProperty, ReactiveList, NullReactiveProperty } from "../Util/ReactiveProperty";
import { Platform, LOKI, Group, Athlete, ClockState } from "../dataTypes";
import { Subject } from "rxjs";
export class ApplicationData implements ReactivePropertyGroup {
    Platforms: ReactiveList<LOKI<Platform>> = new ReactiveList<LOKI<Platform>>();
    Groups: ReactiveList<LOKI<Group>> = new ReactiveList<LOKI<Group>>();
    [key: string]: ReactiveProperty<any>;
}
export class PlatformData implements ReactivePropertyGroup {
    [key: string]: ReactiveProperty<any>;
    ActiveGroupName: NullReactiveProperty<string> = new NullReactiveProperty();
    ActiveAthletes: ReactiveProperty<LOKI<Athlete>[]> = new ReactiveProperty<LOKI<Athlete>[]>([]);
    CurrentAthlete: NullReactiveProperty<LOKI<Athlete>> = new NullReactiveProperty();
    NextAthlete: NullReactiveProperty<LOKI<Athlete>> = new NullReactiveProperty();

    // Judge Decisions    
}
export class ServerTxPlatformStream implements StreamGroup {
    [key: string]: Subject<any>;
    ClockState = new Subject<ClockState>();    
    // Pause Clock state
}
export class ServerRxPlatformStream implements StreamGroup {
    [key: string]: Subject<any>;
    SetActiveGroupName: Subject<string | null> = new Subject();
    RequestClockState: Subject<ClockState> = new Subject();
    // Request Clock state
    // Request Pause Clock state
}