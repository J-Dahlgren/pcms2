import { Athlete, GetAttemptInfo } from "../dataTypes";
export type LiftingOrderFn = (athletes: Athlete[]) => Athlete[];
export function LiftingOrder<A extends Athlete>(athletes: A[]): A[] {
    return athletes.sort(AthleteSortFunction);
}
function AthleteSortFunction(a: Athlete, b: Athlete): number {
    /*
    Sort on discipline
    Attempt Number
    Requested Weight
    Lot
     */
    const aInfo = GetAttemptInfo(a.Lifts);
    const bInfo = GetAttemptInfo(b.Lifts);

    // Compares next lift, Squat first, then Bench, Deadlift
    if (aInfo.Discipline !== bInfo.Discipline) return aInfo.Discipline - bInfo.Discipline;

    // Lower attempt number first
    if (aInfo.AttemptNumberOneIndexed !== bInfo.AttemptNumberOneIndexed) return aInfo.AttemptNumberOneIndexed - bInfo.AttemptNumberOneIndexed;

    // Lower weight first
    if (aInfo.Weight !== bInfo.Weight) return aInfo.Weight - bInfo.Weight;
    // Lower lot first
    return a.Lot - b.Lot;
}