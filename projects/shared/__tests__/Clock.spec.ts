import { async } from "rxjs/internal/scheduler/async";
import { CountdownTimer, Sleep } from "../src";

describe("CountdownTimer", () => {
    it("Starts clock on state change", async () => {
        const clock = new CountdownTimer(50);
        clock.RemainingMillis.Value = 1000;
        clock.Start();
        await Sleep(150);
        expect(clock.RemainingMillis.Value).toBeLessThanOrEqual(900);
        clock.Pause(100);
        clock.Status.Value = "RUNNING";
        await Sleep(150);
        expect(clock.RemainingMillis.Value).toBe(0);
        expect(clock.Status.Value).toBe("PAUSED");
        clock.SetState({ Status: "RUNNING", RemainingMillis: 100 });
        await Sleep(50);
        expect(clock.State.Status).toBe("RUNNING");
    }, 500);

});