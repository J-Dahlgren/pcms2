import { GetAttemptWeight, LiftStatus, Lifts, GetCurrentAttempt, GetAttemptInfo, Discipline, DISCIPLINE } from "../src"

const lifts: Lifts = {
    Squat: [
        {
            Requested: 10,
            Status: LiftStatus.SUCCESFUL
        }
    ],
    Bench: [
        {
            Requested: 10,
            Status: LiftStatus.FAILED
        },
        {
            Requested: 11,
            Status: LiftStatus.NOT_ATTEMPTED
        }
    ],
    Deadlift: []
}
const emptyLifts = { Squat: [], Bench: [], Deadlift: [] };
describe("Lift helpers", () => {
    test("GetAttemptWeight", () => {
        expect(GetAttemptWeight({ Automatic: 0, Requested: 10, Status: LiftStatus.NOT_ATTEMPTED })).toBe(10);
        expect(GetAttemptWeight({ Automatic: 10, Requested: 0, Status: LiftStatus.NOT_ATTEMPTED })).toBe(10);
        expect(GetAttemptWeight({ Status: LiftStatus.NOT_ATTEMPTED })).toBeFalsy();
    })
    test("GetCurrentAttempt", () => {
        const attemptInfo = GetCurrentAttempt(lifts);
        expect(attemptInfo).not.toBeNull();
        expect(attemptInfo.Name).toEqual("Bench");
        expect(attemptInfo.AttemptNumberOneIndexed).toEqual(2);
        expect(attemptInfo.RequestedWeight).toEqual(11);
        const empty = GetCurrentAttempt(emptyLifts)
        expect(empty).toBeNull();
    })
    test("GetAttemptInfo", () => {
        const info1 = GetAttemptInfo(lifts);
        expect(info1.Discipline).toEqual(DISCIPLINE.Bench);
        expect(info1.AttemptNumberOneIndexed).toEqual(2);
        expect(info1.Weight).toEqual(11);
        const info2 = GetAttemptInfo(emptyLifts);
        expect(info2.Discipline).toEqual(DISCIPLINE.Done);
        expect(info2.AttemptNumberOneIndexed).toEqual(Number.MAX_SAFE_INTEGER);
        expect(info2.Weight).toEqual(Number.MAX_SAFE_INTEGER);

    })
})