import { GetAgeCategory, AthleteData, AgeCategories } from "../src";

describe("Athlete", () => {
    test("GetAgeCategory", () => {
        const BornFunc = (age: number) => new Date().getFullYear() - age;
        const age = 19;
        const born = BornFunc(age);
        const Athlete: AthleteData = {
            Firstname: "",
            Lastname: "",
            BornYear: born,
            Lot: 0,
            Gender: "M"
        }
        let cat = GetAgeCategory(Athlete);
        expect(cat).not.toBeNull();
        expect(cat.Name).toBe(AgeCategories.Junior.Name)
        Athlete.BornYear = BornFunc(30);
        cat = GetAgeCategory(Athlete);
        expect(cat.Name).toBe(AgeCategories.Open.Name);
        Athlete.BornYear = BornFunc(5);
        cat = GetAgeCategory(Athlete);
        expect(cat.Name).toBe(AgeCategories.Open.Name);
    })
});