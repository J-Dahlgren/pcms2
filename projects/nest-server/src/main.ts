import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import configuration from './config/configuration';
import { Logger, ValidationPipe } from '@nestjs/common';

async function bootstrap() {

    const logger = new Logger("Bootstrapper");
    logger.debug(`Application config: ${JSON.stringify(configuration(), null, 2)}`);
    const app = await NestFactory.create(AppModule);
    const port = configuration().port;
    await app.listen(port);
    logger.log(`Listening on port ${port}`);
}
bootstrap();
