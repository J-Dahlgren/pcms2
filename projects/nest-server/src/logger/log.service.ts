import { Logger, Injectable, Scope } from "@nestjs/common";
@Injectable({ scope: Scope.TRANSIENT })
export default class LogService extends Logger {
    constructor(context?: string | undefined, isTimestampEnabled?: boolean | undefined) {
        super(context, isTimestampEnabled)
    }
}