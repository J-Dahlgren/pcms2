import { Module, Logger, Scope } from '@nestjs/common';
import LogService from './log.service';

@Module({
    providers: [
        LogService
    ],
    exports: [LogService]
})
export class LoggerModule { }
