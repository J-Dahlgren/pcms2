export default () => ({
    port: parseInt(process.env.PORT + "", 10) || 8080,
    database: {
        name: process.env.DATABASE_NAME,
    }
});