import { Module, Logger, Scope } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from "@nestjs/config";
import { CompetitionModule } from './competition/competition.module';
import { PlatformModule } from './platform/platform.module';
import { CoreModule } from './core/core.module';
import { LoggerModule } from './logger/logger.module';
import configuration from './config/configuration';
@Module({
    imports: [
        ConfigModule.forRoot({
            load: [configuration]
        }),
        TypeOrmModule.forRoot({
            type: "sqlite",
            synchronize: true,
            database: process.env.DATABASE_NAME || "DB/db.sqlite",
            autoLoadEntities: true
        }),
        LoggerModule,
        CompetitionModule,
        PlatformModule,
        CoreModule
    ],
    controllers: [AppController],
    providers: [
        AppService
    ],
})
export class AppModule { }
