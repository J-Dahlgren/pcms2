import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { Entry, ICompetition } from "shared";
import PlatformEntity from "../platform/platform.entity";

@Entity()
export default class CompetitionEntity implements Entry<ICompetition>{
    @PrimaryGeneratedColumn()
    id!: number;
    @Column({ nullable: false })
    name!: string;
    @OneToMany(() => PlatformEntity, platform => platform.competition, {

    })
    platforms!: PlatformEntity[];
}