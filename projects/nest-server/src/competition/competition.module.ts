import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import CompetitionEntity from 'src/competition/competition.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([CompetitionEntity])
    ],
    providers: [

    ],
    controllers: [

    ]
})
export class CompetitionModule { }
