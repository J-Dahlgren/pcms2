import { ICompetition } from "shared";
import { IsNotEmpty } from "class-validator";
import PlatformDto from "src/platform/platform.dto";

export default class CompetitionDto implements ICompetition {
    @IsNotEmpty()
    name: string = "";
    platforms?: PlatformDto[];
}