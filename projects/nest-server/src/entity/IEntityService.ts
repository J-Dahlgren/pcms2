import { Id } from "shared";
import { FilterQuery } from "typeorm";

export interface IEntityService<IEntity extends {}, Entity extends IEntity> {
    create(createEntity: IEntity): Promise<Entity>;
    update(Id: Id, updateEntity: IEntity): Promise<Entity>
    remove(Id: Id): Promise<Entity>;
    findOne(Id: Id): Promise<Entity>
    findAll(query: FilterQuery<Entity>): Promise<Entity[]>
}