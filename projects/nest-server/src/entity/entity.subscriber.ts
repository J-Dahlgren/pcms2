import { EntitySubscriberInterface, InsertEvent, RemoveEvent, UpdateEvent } from "typeorm";
import EntityService from "./entity.service";
import { Logger } from "@nestjs/common";
import { Subject } from "rxjs";
import { filter } from "rxjs/operators";

export default abstract class EntitySubscriber<IEntity extends {}, Entity extends IEntity> implements EntitySubscriberInterface<Entity>{

    constructor(protected logger: Logger) {
        logger.debug("Created");
    }
    abstract listenTo(): Function | string;
    protected block = false;
    private onChange = new Subject<void>();
    public get OnChange() {
        return this.onChange.asObservable().pipe(filter(() => !this.block));
    }
    private insertStream = new Subject<IEntity>();
    public get onInsert() {
        return this.insertStream.asObservable().pipe(filter(() => !this.block));
    }
    private updateStream = new Subject<IEntity>();
    public get onUpdate() {
        return this.updateStream.asObservable().pipe(filter(() => !this.block));
    }
    private removeStream = new Subject<IEntity>();
    public get onRemove(){
        return this.removeStream.asObservable().pipe(filter(() => !this.block));
    }
    protected changed() {
        this.onChange.next();
    }

    public afterInsert(event: InsertEvent<IEntity>) {
        this.insertStream.next(event.entity);
        this.changed();
    }
    public afterUpdate(event: UpdateEvent<IEntity>) {
        this.updateStream.next(event.entity);
        this.changed();
    }
    public afterRemove(event: RemoveEvent<IEntity>) {
        this.removeStream.next(event.entity);
        this.changed();
    }
}
