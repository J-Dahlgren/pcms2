import { Id } from "shared";
import { FilterQuery } from "typeorm";
export interface IEntityController<IEntity extends {}, EntityDto extends IEntity, Entity extends IEntity> {
    create(createDto: EntityDto): Promise<Entity>;
    update(Id: Id, updateDto: EntityDto): Promise<Entity>;
    findOne(Id: Id): Promise<Entity>;
    findAll(query: FilterQuery<IEntity>): Promise<Entity[]>
    remove(Id: Id): Promise<Entity>;
}