import { IEntityService } from "./IEntityService";
import { Id } from "shared";
import { Repository,FilterQuery } from "typeorm";
import { Logger } from "@nestjs/common";

export default abstract class EntityService<IEntity, Entity extends IEntity> implements IEntityService<IEntity, Entity> {
    constructor(protected logger: Logger, protected repo: Repository<Entity>) {
        logger.debug("Created");
    }
    create(createEntity: IEntity): Promise<Entity> {
        const entity = this.repo.create();
        Object.assign(entity, createEntity);
        return this.repo.save(entity);
    }
    async update(Id: Id, updateEntity: IEntity): Promise<Entity> {
        const entity = await this.findOne(Id);
        Object.assign(entity, updateEntity);
        return this.repo.save(entity);
    }
    async remove(Id: Id): Promise<Entity> {
        const entity = await this.findOne(Id);
        return this.repo.remove(entity);
    }
    findOne(Id: Id): Promise<Entity> {
        return this.repo.findOneOrFail(Id);
    }
    findAll(query: FilterQuery<Entity>): Promise<Entity[]> {
        return this.repo.find()
    }
}