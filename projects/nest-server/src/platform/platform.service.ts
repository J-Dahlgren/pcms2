import EntityService from "src/entity/entity.service";
import { IPlatform } from "shared";
import { Injectable } from "@nestjs/common";
import LogService from "src/logger/log.service";
import PlatformEntity from "./platform.entity";
import { Repository } from "typeorm";
import { IEntityService } from "src/entity/IEntityService";
import { InjectRepository } from "@nestjs/typeorm";
import { AppService } from "src/app.service";
export interface IPlatformService extends IEntityService<IPlatform, PlatformEntity> {

}
@Injectable()
export class PlatformService extends EntityService<IPlatform, PlatformEntity> implements IPlatformService {
    constructor(loggService: LogService, @InjectRepository(PlatformEntity) repo: Repository<PlatformEntity>) {
        loggService.setContext(PlatformService.name);
        super(loggService, repo);
    }
}