import { Controller, Get, Inject, Post, Body, Patch, Param, Delete, Query, ValidationPipe, UsePipes } from "@nestjs/common";
import PlatformEntity from "./platform.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { IPlatform } from "shared";
import { PlatformService } from "./platform.service";
import PlatformDto from "./platform.dto";
import { IEntityController } from "src/entity/IEntityController";
import { Id } from "shared";
import { FilterQuery } from "typeorm";
const Resource = "platform";
@Controller(Resource)
export default class PlatformController implements IEntityController<IPlatform, PlatformDto, PlatformEntity> {

    constructor(@InjectRepository(PlatformEntity) repo: Repository<PlatformEntity>, private entityService: PlatformService) {
    }
    @Get(":id")
    findOne(@Param("id") Id: Id): Promise<PlatformEntity> {
        return this.entityService.findOne(Id);
    }
    @Get()
    findAll(@Query() query: any): Promise<PlatformEntity[]> {
        return this.entityService.findAll(query as FilterQuery<IPlatform>);
    }
    @Post()
    @UsePipes(new ValidationPipe())
    create(@Body() createDto: PlatformDto) {
        return this.entityService.create(createDto);
    }
    @Patch(":id")
    @UsePipes(new ValidationPipe({ skipMissingProperties: true }))
    update(@Param("id") Id: Id, @Body() updateDto: PlatformDto): Promise<PlatformEntity> {
        return this.entityService.update(Id, updateDto);
    }

    @Delete(":id")
    remove(@Param("id") Id: Id): Promise<PlatformEntity> {
        return this.entityService.remove(Id);
    }
}