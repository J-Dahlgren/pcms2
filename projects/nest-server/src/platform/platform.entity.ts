import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { IPlatform, Entry, Id } from "shared";
import CompetitionEntity from "../competition/competition.entity";

@Entity()
export default class PlatformEntity implements Entry<IPlatform>{
    @PrimaryGeneratedColumn()
    id!: number;
    @Column({ nullable: false })
    name!: string;
    @Column()
    competitionId!: Id;
    @ManyToOne(() => CompetitionEntity, comp => comp.platforms, {
        nullable: false
    })
    competition!: CompetitionEntity;
}