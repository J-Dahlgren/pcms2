import { Module, Scope, Logger } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import PlatformEntity from 'src/platform/platform.entity';
import PlatformSubscriber from './platform.subscriber';

import { LoggerModule } from 'src/logger/logger.module';
import PlatformController from './platform.controller';
import { PlatformService } from './platform.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([PlatformEntity]),
        LoggerModule
    ],
    providers: [
        PlatformService        
    ],
    exports: [
        PlatformService        
    ],
    controllers: [
        PlatformController
    ]
})
export class PlatformModule { }
