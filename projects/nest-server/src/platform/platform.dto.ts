import { IPlatform, Id } from "shared";
import { IsNotEmpty, IsNumberString } from "class-validator";

export default class PlatformDto implements IPlatform {
    @IsNotEmpty()
    name: string = "";
    @IsNotEmpty()
    competitionId: Id = 0;
}
