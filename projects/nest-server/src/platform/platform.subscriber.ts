import EntitySubscriber from "src/entity/entity.subscriber";

import PlatformEntity from "./platform.entity";
import LogService from "src/logger/log.service";
import { EventSubscriber } from "typeorm";
import PlatformDto from "./platform.dto";
@EventSubscriber()
export default class PlatformSubscriber extends EntitySubscriber<PlatformDto, PlatformEntity>{
    constructor(logger: LogService) {
        logger.setContext(PlatformSubscriber.name);
        super(logger);
    }
    listenTo(): string | Function {
        return PlatformEntity;
    }
}