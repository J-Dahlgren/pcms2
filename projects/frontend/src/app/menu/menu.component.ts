import { OnInit, Component, Input } from "@angular/core";
import { NavService } from "../core/nav.service";
import { ApiService } from "../core/api.service";
import { map } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";

@Component({
    selector: "app-menu",
    templateUrl: "./menu.component.html",
    styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
    @Input() appName: string;
    @Input() connected = false;
    constructor(private dialog: MatDialog, public navService: NavService, private apiService: ApiService) {

    }

    ngOnInit() {
    }
    ToggleExpanded() {
        this.navService.ToggleExpanded();
    }
    OpenInfo() {
        // this.apiService.ReadAll<string>("info")
        //     .pipe(map(Response => Response.Data))
        //     .subscribe(
        //         data => this.dialog.open(AppInfoDialogComponent, { hasBackdrop: true, data: { appName: this.appName, NetInterfaces: data } }),
        //         () => { }
        //     );
    }
}
