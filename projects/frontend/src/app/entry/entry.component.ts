import { Component, OnInit, Input, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { NavItem } from "../menu-list-item/nav-item";
import { ApiService } from "../core/api.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SocketService } from "../core/socket.service";
import { NavService } from "../core/nav.service";
import { LoadService } from "../core/load.service";
import { AppDataService } from "../core/app-data.service";
import { LOKI, Platform } from "shared";

@Component({
    selector: "app-entry",
    templateUrl: "./entry.component.html",
    styleUrls: ["./entry.component.scss"]
})
export class EntryComponent implements OnInit, AfterViewInit {

    @Input() appName = "";
    @Input() navItems: NavItem[] = [];
    public connected = false;
    public api: ApiService;
    loading = false;

    constructor(
        public apiService: ApiService,
        private snackBar: MatSnackBar,
        private socketService: SocketService,
        public navService: NavService,
        private loadService: LoadService,
        public appDataService: AppDataService,
        private cdfRef: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.socketService.connected.pipe(
            // tap(connection => console.log("Connected", connection))
        ).subscribe(connection => {
            if (this.connected !== connection) { // State change, display snackbar
                const message = connection ? "Connected to server" : "Lost connection to server";
                const panelClass = connection ? "snack-bar-success" : "snack-bar-failed";
                const duration = connection ? 2500 : undefined;
                this.snackBar.open(message, "Dismiss", { duration, panelClass });
            }
            this.connected = connection;
        });

    }
    ngAfterViewInit(): void {
        this.loadService.Loading.subscribe(loading => {
            this.loading = loading;
            this.cdfRef.detectChanges();
        });
    }
    backdropClick() {
        this.navService.Collapse();
    }
    SelectPlatform(platform: LOKI<Platform> | null) {
        // this.platformService.ActivePlatform = platform;
        // this.platformService.Plates.Value = platform.Plates;
    }
}
