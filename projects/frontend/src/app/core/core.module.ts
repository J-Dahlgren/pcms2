import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EnvironmentService } from "./environment.service";
import { ApiService } from "./api.service";
import { LoadService } from "./load.service";
import { SocketService } from "./socket.service";
import { PlatformService } from "./platform.service";
import { AppDataService } from "./app-data.service";
import { NavService } from "./nav.service";



@NgModule({
    declarations: [],
    imports: [
        CommonModule,

    ],
    providers: [
        EnvironmentService,
        LoadService,
        ApiService,
        SocketService,
        AppDataService,
        PlatformService,
        NavService
    ],
    exports: [
    ]
})
export class CoreModule { }
