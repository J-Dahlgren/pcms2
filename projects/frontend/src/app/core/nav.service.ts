import { Injectable } from "@angular/core";
import { Event, NavigationEnd, Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
@Injectable({
    providedIn: "root"
})
export class NavService {
    public NavExpanded = false;
    public Visible = true;
    private loading = new BehaviorSubject<boolean>(false);
    private loadCounter = 0;
    public get Loading() {
        return this.loading.asObservable();
    }
    public currentUrl = new BehaviorSubject<string>(undefined);
    constructor(private router: Router) {
        this.router.events.subscribe((event: Event) => {
            if (event instanceof NavigationEnd) {
                this.currentUrl.next(event.urlAfterRedirects);
            }
        });
    }
    ShowLoading() {
        this.loadCounter++;
        this.loading.next(true);
    }
    HideLoading() {
        this.loadCounter--;
        if (this.loadCounter <= 0) {
            this.loadCounter = 0;
            this.loading.next(false);
        }
    }
    SetVisibility(visible: boolean) {
        this.Visible = visible;
    }
    ExpandNav() {
        this.NavExpanded = true;
    }
    Collapse() {
        this.NavExpanded = false;
    }
    ToggleExpanded() {
        this.NavExpanded = !this.NavExpanded;
    }

}
