import { Injectable } from "@angular/core";
import { SocketService } from "./socket.service";
import { SocketClientRoom } from "./client-data-group";
import { PlatformData, ServerTxPlatformStream, ServerRxPlatformStream } from "shared";

@Injectable({
    providedIn: "root"
})
export class PlatformService {
    private socketClientRoom: SocketClientRoom<PlatformData, ServerTxPlatformStream, ServerRxPlatformStream>;
    public readonly Data: PlatformData;
    public readonly RxEvents: ServerTxPlatformStream;
    public readonly TxEvents: ServerRxPlatformStream;
    constructor(socketService: SocketService) {
        this.socketClientRoom = new SocketClientRoom(
            "",
            new PlatformData(),
            new ServerTxPlatformStream(),
            new ServerRxPlatformStream(),
            socketService.Socket);
        this.Data = this.socketClientRoom.PropGroup;
        this.RxEvents = this.socketClientRoom.RxStream;
        this.TxEvents = this.socketClientRoom.TxStream;
    }
}
