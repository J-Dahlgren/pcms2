import { LoadService } from "./load.service";

describe("LoadService", () => {
    let loadService: LoadService;
    beforeEach(() => loadService = new LoadService());
    it("Emits loading boolean", async () => {
        let fn = jest.fn();
        loadService.Loading.subscribe(loading => fn());
        loadService.ShowLoading();
        loadService.HideLoading();
        loadService.HideLoading();
        expect(fn).toBeCalledTimes(3);
    }, 100);
});
