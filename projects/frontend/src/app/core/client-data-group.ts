import { ReactivePropertyGroup, StreamGroup, DataGroupRoom, IDisposable, RoomData, CLIENT_AUTH_REQUEST, DataGroup } from "shared";

import { Subscription } from "rxjs";

export class SocketClientRoom<RPG extends ReactivePropertyGroup,
    RxStream extends StreamGroup,
    TxStream extends StreamGroup> extends DataGroupRoom<RPG, RxStream, TxStream> implements IDisposable {
    protected Subs: Subscription[] = [];
    Dispose() {
        this.Subs.forEach(sub => sub.unsubscribe());
        this.Subs = [];
    }
    constructor(Room: string, syncData: RPG, rxStream: RxStream, txStream: TxStream, private client: SocketIOClient.Socket) {
        super(Room, syncData, rxStream, txStream);
        for (const key in syncData) {
            client.on(key, (roomData?: RoomData) => {
                if (roomData && roomData.Room === this.Room) {
                    this.PropGroup[key].Value = roomData.Data;
                }
            });
        }

        for (const key in rxStream) { // Listen to client and emit on Rx stream
            client.on(key, (roomData?: RoomData) => {
                if (roomData && roomData.Room === this.Room) {
                    rxStream[key].next(roomData.Data);
                }
            });
        }
        for (const key in txStream) {  // Subscribe to Tx Events and emit to server
            this.Subs.push(txStream[key].subscribe(next => this.client.emit(key, this.CreateRoomData(next))));
        }
    }
    public TransmitToken(token: string) {
        this.client.emit(CLIENT_AUTH_REQUEST, token);
    }
}


export class SocketClientDataGroup<T extends ReactivePropertyGroup> extends DataGroup<T> {
    constructor(client: SocketIOClient.Socket, syncData: T) {
        super(syncData, {}, {});
        for (const key in syncData) {
            client.on(key, (data: any) => syncData[key].Value = data);
        }
    }
}
