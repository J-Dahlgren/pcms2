import { Injectable } from "@angular/core";
import { connect } from "socket.io-client";
import { IEnvironment } from "../../environments/IEnvironment";
import { environment } from "../../environments/environment";

@Injectable({
    providedIn: "root"
})
export class EnvironmentService implements IEnvironment {
    public readonly production = environment.production;
    public readonly socketPort = environment.socketPort;

    constructor() {

    }
}
