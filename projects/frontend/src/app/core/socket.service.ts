import { Injectable } from "@angular/core";
import { EnvironmentService } from "./environment.service";
import { BehaviorSubject } from "rxjs";
import { connect } from "socket.io-client";
@Injectable({
    providedIn: "root"
})
export class SocketService {
    protected socket: SocketIOClient.Socket;
    private connectionStatus: BehaviorSubject<boolean> = new BehaviorSubject(false);

    public get connected() {
        return this.connectionStatus.asObservable();
    }
    public get Socket() {
        return this.socket;
    }
    constructor(envService: EnvironmentService) {
        const socket = connect(`${window.location.hostname}:${envService.socketPort}/`, { forceNew: true });
        this.socket = socket;
        socket.on("connect", () => this.connectionStatus.next(true));
        socket.on("connect_error", () => this.connectionStatus.next(false));
    }
}
