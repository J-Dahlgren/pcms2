import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class LoadService {
    private loading = new BehaviorSubject<boolean>(false);
    private loadCounter = 0;
    public get Loading() {
        return this.loading.asObservable();
    }
    constructor() {

    }
    ShowLoading() {
        this.loadCounter++;
        this.loading.next(true);
    }
    HideLoading() {
        this.loadCounter--;
        if (this.loadCounter < 0) {
            this.loadCounter = 0;
            this.loading.next(false);
        }
    }
}
