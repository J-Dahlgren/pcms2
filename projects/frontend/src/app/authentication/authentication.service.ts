import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
@Injectable({
    providedIn: "root"
})
export class AuthenticationService {
    public get IsLoggedIn() {
        if (!environment.production) {
            return true;
        }
        return false;
    }
    constructor() {

    }
}
