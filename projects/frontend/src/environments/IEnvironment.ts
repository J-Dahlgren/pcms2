export interface IEnvironment {
    production: boolean;
    socketPort: number;
}
