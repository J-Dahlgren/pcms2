module.exports = {
    preset: "jest-preset-angular",
    testRegex: "(projects/frontend/src/app/.*(test|spec))\\.ts$",    
    setupFilesAfterEnv: [
        "<rootDir>/test.ts"
    ]
}
